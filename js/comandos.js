//Aplicando eventos diretamente no JS(addEventListener)
window.addEventListener("load", function(){

    
    botaoUm = document.getElementById("btnUm");
    textoEscondidoUm = this.document.getElementById("textoEscondidoUm");
    atributoUm = botaoUm.getAttribute("aria-expanded");

    botaoDois = document.getElementById("btnDois");
    textoEscondidoDois = this.document.getElementById("textoEscondidoDois");
    atributoDois = botaoDois.getAttribute("aria-expanded");

    botaoTres = document.getElementById("btnTres");
    textoEscondidoTres = this.document.getElementById("textoEscondidoTres");
    atributoTres = botaoDois.getAttribute("aria-expanded");


    this.console.log($(botaoDois).attr("aria-expanded"))


    var numeroClicksUm = 0;
    var numeroClicksDois = 0;
    var numeroClicksTres = 0;

    //Verifica se o atributo aria-expanded está true
    $(botaoDois).click(function() {
        //numeroClicksDois = 1;

        if((numeroClicksDois) == 0){
            numeroClicksDois = 1;
            $(textoEscondidoDois).show();
        }else{
            numeroClicksDois = 0;
            $(textoEscondidoDois).hide();
        }
    });


    $(botaoUm).click(function() {
        //numeroClicksUm++;

        if((numeroClicksUm) == 0){
            numeroClicksUm = 1;
            $(textoEscondidoUm).show();
        }else{
            numeroClicksUm = 0;
            $(textoEscondidoUm).hide();
        }
    });


    $(botaoTres).click(function() {
        numeroClicksTres++;

        if((numeroClicksTres%2) == 0){
            $(textoEscondidoTres).hide();
        }else{
            $(textoEscondidoTres).show();
        }
    });


    var imagemEstrela = this.document.getElementById('star');
    var logo = this.document.getElementById('logo');
    var imagemLampada = this.document.getElementById('idea');


    var escreverTitulo = document.getElementById('tituloEscreve');
    var titulo = '"Trazendo o Universo para sala de aula!"';


    var escreverSubtitulo = document.getElementById('subtituloEscreve');
    var subtitulo;

    if($("title").text() == "ASTROEM"){
        subtitulo =  'ASTROEM é uma entidade que busca descobrir e espalhar o conhecimento.'
    } else if ($("title").text() == "ASTROEM - GALERIA"){
        subtitulo =  'Aqui você poderá ver todas nossas mídias visuais de nossas aventuras!'
    } else if($("title").text() == "ASTROEM - HISTÓRIA"){
        subtitulo = 'Aqui estão nossos arquivos mais secretos. Estão prontos?';
    }

   var escreverSubtituloDois = document.getElementById('subtituloEscreveDois');
   var subtituloDois;


   if($("title").text() == "ASTROEM"){
    subtituloDois =  'Com alunos e docentes apaixonados pela pesquisa espacial, nosso objetivo é repassar essa paixão para os pontenciais exploradores interessados! Descubra mais sobre nossos projetos e embarque nessa jornada você também!';
    } else if ($("title").text() == "ASTROEM - GALERIA"){
        subtituloDois =  'Bora embarcar nessa?'
    } 


        
        function escreve(str, el) {
              var char = str.split('').reverse();
              var typer = setInterval(function() {
           
            if (!char.length)
                return clearInterval(typer);

            var next = char.pop();
            el.innerHTML += next;
          }, 100);
        }


        function escreveRapido(str, el) {
            var char = str.split('').reverse();
            var typer = setInterval(function() {
         
          if (!char.length)
              return clearInterval(typer);

          var next = char.pop();
          el.innerHTML += next;
        }, 50);
      }


        escreve(titulo, escreverTitulo);

        setTimeout(function(str, el) {
            escreveRapido(subtitulo, escreverSubtitulo);
        }, 5000);


        setTimeout(function(str, el) {
            escreveRapido(subtituloDois, escreverSubtituloDois);
        }, 9000);




    logo.onmouseover = function(){

        //logo.style.setProperty("-webkit-transition-delay", "0, 2s");
        //logo.style.width = "27%";
        logo.style.filter = "brightness(150%)"
    }


    logo.onmouseout = function(){

        logo.style.filter = "brightness(60%)"

    }


    imagemEstrela.onmouseover = function(){

        imagemEstrela.style.filter = "grayscale(0%)";
        imagemEstrela.style.webkitFilter = "grayscale(0%)";

    }


    imagemEstrela.onmouseout = function(){

        imagemEstrela.style.filter = "grayscale(100%)";
        imagemEstrela.style.webkitFilter = "grayscale(100%)";

    }



    imagemLampada.onmouseover = function(){
        imagemLampada.style.filter = "grayscale(0%)";
        imagemLampada.style.webkitFilter = "grayscale(0%)";
        imagemLampada.style.filter = "brightness(150%)";
    }


    imagemLampada.onmouseout = function(){
        imagemLampada.style.filter = "grayscale(100%)";
        imagemLampada.style.webkitFilter = "grayscale(100%)";
        //idea.style.filter = "brightness(60%)";

    }

    //Mudando a cor das frases

    var aryBox = document.getElementById("aryanne");
    var aryFrase = document.getElementById("aryanneText");
    var aryNome = document.getElementById("aryanneNome");
   
    var irisBox = document.getElementById("iris");
    var irisNome = document.getElementById("irisNome");
    var irisFrase = document.getElementById("irisText");
   
    var guiBox = document.getElementById("guilherme");
    var guiFrase = document.getElementById("guilhermeText");
    var guiNome = document.getElementById("guilhermeNome");
   
    var gabrielBox = document.getElementById("gabriel");
    var gabrielFrase = document.getElementById("gabrielText");
    var gabrielNome = document.getElementById("gabrielNome");
    var gabrielFundo = document.getElementById("fundoGabriel");
    var gabrielFoto = document.getElementById("fotoGabriel");
   
    var luisBox = document.getElementById("luis");
    var luisFrase = document.getElementById("luisText");
    var luisNome = document.getElementById("luisNome");


    aryBox.onmouseover = function(){
        aryNome.style.backgroundColor = "transparent";
        aryNome.style.color = "white";
        aryFrase.classList.remove("text-warning");
        aryFrase.style.color = "white";
        aryFrase.style.backgroundColor = "#ffc107";
        aryBox.style.backgroundColor = "#ffc107";

    }

    aryBox.onmouseout = function(){
        aryFrase.classList.add("text-warning");
        aryFrase.style.backgroundColor = "transparent";
        aryBox.style.backgroundColor = "transparent";
        aryNome.style.backgroundColor = "transparent";

    }


    irisBox.onmouseover = function(){
        
        irisNome.style.backgroundColor = "transparent";
        irisNome.style.color = "white";
        irisFrase.classList.remove("text-danger");
        irisFrase.style.color = "white";
        irisFrase.style.backgroundColor = "#dc3545";
        irisBox.style.backgroundColor = "#dc3545";

    }

    irisBox.onmouseout = function(){
        
        irisFrase.classList.add("text-danger");
        irisFrase.style.backgroundColor = "transparent";
        irisBox.style.backgroundColor = "transparent";
        irisNome.style.backgroundColor = "transparent";

    }

    gabrielBox.onmouseover = function(){
        
        gabrielNome.style.backgroundColor = "transparent";
        gabrielNome.style.color = "#6c757d";
        gabrielFrase.classList.remove("text-secondary");
        gabrielFrase.style.color = "white";
        gabrielFrase.style.backgroundColor = "#6c757d";
        gabrielBox.style.backgroundColor = "#6c757d";

    }

    gabrielBox.onmouseout = function(){
        
        gabrielFrase.classList.add("text-secondary");
        gabrielNome.style.color = "black";
        gabrielFrase.style.backgroundColor = "transparent";
        gabrielBox.style.backgroundColor = "transparent";
        gabrielNome.style.backgroundColor = "transparent";

    }


    gabrielFoto.onmouseover = function(){
        gabrielFundo.style.visibility = "visible";	
    }


    gabrielFoto.onmouseout = function(){
        gabrielFundo.style.visibility = "hidden";	
    }


    luisBox.onmouseover = function(){
        luisNome.style.backgroundColor = "transparent";
        luisNome.style.color = "white";
        luisFrase.classList.remove("text-primary");
        luisFrase.style.color = "white";
        luisFrase.style.backgroundColor = "#007bff";
        luisBox.style.backgroundColor = "#007bff";

    }

    luisBox.onmouseout = function(){
        luisFrase.classList.add("text-primary");
        luisFrase.style.backgroundColor = "transparent";
        luisBox.style.backgroundColor = "transparent";
        luisNome.style.backgroundColor = "transparent";

    }


    guiBox.onmouseover = function(){
        guiNome.style.backgroundColor = "transparent";
        //guiNome.style.color = "white";
        guiNome.style.color = "#28a745";
        guiFrase.classList.remove("text-success");
        guiFrase.style.color = "white";
        guiFrase.style.backgroundColor = "#28a745";
        guiBox.style.backgroundColor = "#28a745";

    }

    guiBox.onmouseout = function(){
        guiFrase.classList.add("text-success");
        guiFrase.style.backgroundColor = "transparent";
        guiBox.style.backgroundColor = "transparent";
        guiNome.style.backgroundColor = "transparent";
        guiNome.style.color = "black";

    }




    


});
